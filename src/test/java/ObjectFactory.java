import contactsbook.model.Person;

import java.util.List;

/**
 * Created by harsh on 5/8/16.
 */
public class ObjectFactory {

    public static Person createPersonObject(String fName, String lName) {

       Person person = new Person();

        person.setFirstName(fName);
        person.setLastName(lName);

        return person;
    }
}
