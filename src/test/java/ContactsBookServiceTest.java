import contactsbook.model.Person;
import contactsbook.service.ContactsBookService;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by harsh.
 */

@RunWith(JUnit4.class)
public class ContactsBookServiceTest extends TestCase {


    private ContactsBookService contactsBookService;

    private ObjectFactory objectFactory;

    @Before
    public void setUp() {
        contactsBookService = new ContactsBookService();
    }

    @Test
    public void shouldAddPersonToContactsList() {


        Person p1 = objectFactory.createPersonObject("test1", "person");
        Person p2 = objectFactory.createPersonObject("test2", null);
        Person p3 = objectFactory.createPersonObject("test3", "person");

        contactsBookService.addToPhoneBook(p1);
        contactsBookService.addToPhoneBook(p2);
        contactsBookService.addToPhoneBook(p3);

        assertThat(contactsBookService.contacts, is(not(empty())));
        assertThat(contactsBookService.contacts, hasSize(3));
    }

    @Test
    public void shouldAddAndSearchInContacts() {

        Person p1 = objectFactory.createPersonObject("harsh", "jain");
        Person p2 = objectFactory.createPersonObject("harsh", null);
        Person p3 = objectFactory.createPersonObject("gurinder", "singh");

        contactsBookService.addToPhoneBook(p1);
        contactsBookService.addToPhoneBook(p2);
        contactsBookService.addToPhoneBook(p3);

        List<Person> testList1 = contactsBookService.searchByName("har"); // by any string
        List<Person> testList2 = contactsBookService.searchByName("sin"); // by last name
        List<Person> testList3 = contactsBookService.searchByName("harsh"); // by first full name

        // list should not be empty
        assertThat(testList1, is(not(empty())));
        assertThat(testList2, is(not(empty())));
        assertThat(testList3, is(not(empty())));

        // returning exact search results
        assertThat(testList1, hasSize(2));
        assertThat(testList2, hasSize(1));
        assertThat(testList3, hasSize(2));


        //list should return expected result
        // as of now only testing for few expected results , we can extend this testing scope
        assertThat(testList1.get(0), is(equalTo(p1)));
        assertThat(testList1.get(1), is(equalTo(p2)));

        assertThat(testList2.get(0), is(equalTo(p3)));

        assertThat(testList3.get(1), is(equalTo(p2)));
        assertThat(testList3.get(0), is(equalTo(p1)));

    }


}