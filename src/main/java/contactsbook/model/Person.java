package contactsbook.model;

import lombok.*;

/**
 * Created by harsh.
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String firstName;
    private String lastName;

    @Override
    public String toString(){
        String fName = firstName != null ? firstName : "";
        String lName = lastName != null ? lastName : "";

        return fName + " " + lName;
    }
}
