package contactsbook.service;

import contactsbook.model.Person;
import lombok.NoArgsConstructor;

import java.security.cert.PKIXRevocationChecker;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Created by harsh.
 */
@NoArgsConstructor
public class ContactsBookService {

    public List<Person> contacts = new ArrayList<>();
    private List<Person> sortedContacts;
    private String sequence;

    public boolean addToPhoneBook(Person name) {

        return contacts.add(name) ? true : false;
    }

    public void printAll() {
        contacts.stream()
                .sorted((p1, p2) -> p1.getFirstName().toLowerCase().compareTo(p2.getFirstName().toLowerCase()))
                .forEach(System.out::println);
    }

    // searching for input string pattern
    public List<Person> searchByName(String sequence) {

        this.sequence = sequence.toLowerCase();

        sortedContacts = contacts.stream()
                .filter(this::filterByName)
                .sorted((p1, p2) -> p1.getFirstName().toLowerCase().compareTo(p2.getFirstName().toLowerCase()))
                .collect(toList());

        return sortedContacts;
    }

    private boolean filterByName(Person person) {
        boolean flag = false;
        if (checkSequence(person))
            flag = true;

        return flag;
    }

    // checking sequence
    private boolean checkSequence(Person person) {

        Optional<String> fName = Optional.ofNullable(person.getFirstName());
        Optional<String> lName = Optional.ofNullable(person.getLastName());
        Predicate<Person> p1 = i -> fName.map(String::toLowerCase).orElse("").contains(sequence)
                || lName.map(String::toLowerCase).orElse("").contains(sequence);

        return p1.test(person);
    }
}
