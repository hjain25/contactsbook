package contactsbook.controller;

import contactsbook.model.Person;
import contactsbook.service.ContactsBookService;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.*;

/**
 * Created by harsh.
 */
public class ContactsBookController {

    ContactsBookService contactsService = new ContactsBookService();
    Person person;

    public static void main(String[] args) {


        ContactsBookController controller = new ContactsBookController();

        while (true) {

            out.println("1) Add Contact 2) Search 3) Exit");
            Scanner sc = new Scanner(in);


            Integer choice = sc.nextInt();

            if (choice.equals(1)) {
                controller.addName();
            } else if (choice.equals(2)) {
                controller.searchName();
            } else if (choice.equals(3)) {
                controller.exit();
//                break;
            } else {
                out.println("Warning : Please enter valid operation type");
            }
        }
    }

    //exiting
    private void exit() {
        out.println("Happy Searching : ");
        contactsService.printAll();
    }

    // searching by any pattern in names
    private void searchName() {

        out.println("Enter name to search : ");
        Scanner name = new Scanner(in);

        List<Person> foundContacts = contactsService.searchByName(name.nextLine());

        if(!foundContacts.isEmpty()){
            foundContacts.forEach(out::println);
        }else {
            out.println("No Match found");
        }
    }

    // adding
    private void addName() {

        out.println("Enter name to add to contacts : ");

        Scanner name = new Scanner(in);
        String[] names = name.nextLine().split(" ");

        person = new Person();

        if (names.length == 2) {
            person.setFirstName(names[0]);
            person.setLastName(names[1]);
        } else {
            person.setFirstName(names[0]);
        }

        if (contactsService.addToPhoneBook(person))
            out.println("person successfully added to contacts");
    }
}


